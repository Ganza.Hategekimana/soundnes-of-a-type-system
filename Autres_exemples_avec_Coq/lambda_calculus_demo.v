Inductive lambdacvars : Type := 
| void : lambdacvars
| cons (v : lambdacvar) (s : lambdacvars) : lambdacvars.

Notation "x :: l" := (cons x l).

Eval compute in void.
Eval compute in lx::lf::void.

Definition Lf := lf::void.
Definition Lg := lg::void.
Definition Lh := lh::void.
Definition Lx := lx::void.
Definition Ly := ly::void.
Definition Lz := lz::void.

Inductive Lcvar : Type :=
| lf : Lcvar
| lg : Lcvar
| lh : Lcvar
| lx : Lcvar
| ly : Lcvar
| lz : Lcvar.

Inductive Lc : Type :=
| var (v : Lcvar) : Lc
| L (v : Lcvar) (l : Lc) : Lc
| exp (l1 : Lc) (l2 : Lc) : Lc.

Eval compute in lf.
Eval compute in var lf.
Eval compute in L (lx) (var lx).
Eval compute in exp (L (lx) (var lx)) (var ly).
Eval compute in L (lx) (L ly (var lx)).

Definition Lequal (v1 v2 : Lcvar) (l : Lc) :=
    match v1, v2 with
    | lf, lf => l
    | lg, lg => l
    | lh, lh => l
    | lx, lx => l
    | ly, ly => l
    | lz, lz => l
    | a, b => (var a)
    end.

Eval compute in Lequal lf lg.
Eval compute in Lequal lg lf.

Definition Lf := var lf.
Definition Lg := var lg.
Definition Lh := var lh.
Definition Lx := var lx.
Definition Ly := var ly.
Definition Lz := var lz.

(*EVAL*)

Fixpoint Lsubstitute (l : Lc) (a : Lcvar) (b : Lc) : Lc :=
    match b with
    | var p => (Lequal p a l)
    | L v la => L v (Lsubstitute l a la)
    | exp e1 e2 => exp (Lsubstitute l a e1) (Lsubstitute l a e2)
    end.

Fixpoint evalL (l : Lc) : Lc :=
    match l with
    | var v => var v
    | L v la => L v (evalL la)
    | exp (var va) la => exp (var va) (evalL la)
    | exp (L a b) la => Lsubstitute la a b
    | exp e1 e2 => exp (evalL e1) (evalL e2)
    end.

Eval compute in evalL(Lx).

Eval compute in evalL(L lx (Lx)).

Eval compute in evalL(exp (L lx (Lx)) (Lz)).

Eval compute in evalL(exp (L lx (Lx)) (L ly (Ly))).

Eval compute in evalL(exp (L lx (Lx)) (Ly)).

Eval compute in evalL(exp (L lx (Lx)) (Ly)).

Eval compute in evalL(exp (L lf (L lx (Lx))) (Ly)).


Inductive nat : Type :=
| z : nat
| S : nat -> nat.

Definition un := S z.
Definition deux := S un.
Definition trois := S deux.
Definition quatre := S trois.
Definition cinq := S quatre.
Definition six := S cinq.
Definition sept := S six.
Definition huit := S sept.
Definition neuf := S huit.
Definition dix := S neuf.

Fixpoint evalLStep (i : nat) (l : Lc) : Lc :=
    match i with
    | z => l
    | S p => evalLStep p (evalL l)
    end.


Definition zero := (L lf (L lx (Lx))).

Definition one := (L lf (L lx (exp (Lf) (Lx)))).

Definition succ := (L lh (L lf (L lx (exp (Lf) (exp (exp (Lh) (Lf)) (Lx)))))).

Eval compute in exp succ zero.

Eval compute in evalL(exp succ zero).

Eval compute in evalL(evalL(exp succ zero)).

Eval compute in evalL(evalL(evalL(exp succ zero))).

Eval compute in evalL(evalL(evalL(evalL(exp succ zero)))).

