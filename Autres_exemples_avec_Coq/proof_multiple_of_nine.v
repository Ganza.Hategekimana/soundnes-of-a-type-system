(*Définition de l'ensemble des multiples de 9*)

(*Fonction recursive*)
Fixpoint mul_nine (n : nat) : bool :=
    match n with
    | 0 => true
    | S(S(S(S(S(S(S(S(S(np))))))))) => mul_nine np
    | _ => false
    end.

(*Type inductif*)
Inductive mulnine : nat -> Prop :=
| mulnine_O : mulnine O
| mulnine_S : forall (n : nat), mulnine n -> mulnine (S(S(S(S(S(S(S((S(S
        n)))))))))).

(*----------------------------------*)

(*Définitions simples*)
Fixpoint pow (x y : nat) : nat :=
    match y with
    | O => 1
    | S n => x * (pow x n)
    end.

Fixpoint pred (n : nat) : nat :=
    match n with
    | O => O
    | S n => n
    end.

(*Associativité de l'addition 1*)
Lemma add_S : forall (n m : nat), m + (S n) = S (m + n).
Proof.
    intros n m.
    induction m.
    simpl.
    reflexivity.
    simpl.
    rewrite IHm.
    reflexivity.
Qed.

(*Associativité de l'addition 2*)
Lemma add_S2 : forall (n m : nat), S (m + n) = (S m) + n.
Proof.
    intros n m.
    induction m.
    simpl.
    reflexivity.
    simpl.
    reflexivity.
Qed.

(*l'addition avec la fonction pred*)
Lemma add_S3 : forall (n m : nat), S (pred n + m) = (S (pred n)) + m.
Proof.
    intros n m.
    induction m.
    reflexivity.
    simpl.
    reflexivity.
Qed.

(*commutativité de l'addition*)
Lemma comm_of_add : forall (n m : nat), n + m = m + n.
Proof.
    intros n m.
    induction n.
    simpl.
    induction m.
    simpl.
    reflexivity.
    simpl.
    apply f_equal.
    apply IHm.
    simpl.
    rewrite IHn.
    rewrite add_S.
    reflexivity.
Qed.

(*Condition d'utilisation pour la fonction pred (en effet, on ne peut l'utiliser que pour n > 0)*)
Lemma S_pred : forall (n : nat), gt n 0 -> S (pred n) = n.
Proof.
    intros n H.
    induction n.
    simpl.
    inversion H.
    simpl.
    reflexivity.
Qed.

(*autre passage important*)
Lemma pred_of_addition_ten : forall (n m : nat), n > 0 -> pred (n + m) = (pred n) + m.
Proof.
    intros n m H.
    induction m.
    simpl.
    induction n.
    inversion H.
    simpl. reflexivity.
    simpl.
    rewrite add_S.
    simpl.
    rewrite add_S.
    simpl.
    rewrite add_S2.
    rewrite S_pred.
    reflexivity.
    apply H.
Qed.

(*autre passage important*)
Lemma x_nine_times : forall (x : nat), (x+(x+(x+(x+(x+(x+(x+(x+(x+0))))))))) = (9*x).
Proof.
    intro x.
    induction x.
    simpl. reflexivity.
    simpl. reflexivity.
Qed.

(*autre passage important*)
Lemma x_ten_times : forall (x : nat), x+(x+(x+(x+(x+(x+(x+(x+(x+(x+0))))))))) = (10*x).
Proof.
    intro x.
    induction x.
    simpl. reflexivity.
    simpl. reflexivity.
Qed.

(*passage un peu honteux*)
Lemma mulnine_add : forall (n m : nat), mulnine n -> mulnine m -> mulnine (m + n).
Admitted.

(*autre passage important*)
Lemma S_9 : forall (n : nat), S(S(S(S(S(S(S(S(S n)))))))) = 9 + n.
Proof.
    induction n.
    simpl. reflexivity.
    rewrite IHn.
    simpl.
    reflexivity.
Qed.

(*autre passage important*)
Lemma mulnine_mulnine : forall (n : nat), mulnine (9*n).
Proof.
    intro n.
    induction n.
    simpl. 
    assert (H0 : mulnine (S(S(S(S(S(S(S(S(S n)))))))))).
    simpl.
    constructor.
    simpl.
    repeat rewrite add_S.
    rewrite x_nine_times.
    constructor.
    apply IHn.
Qed.

(*autre passage un peu honteux*)
Lemma pow_ten_gt_zero : forall (n : nat), pow 10 n > 0.
Admitted.

(*Preuve final*)
Theorem power_ten_minus_one : forall (n : nat), n > 0 -> mulnine (pred (pow 10 n)).
Proof.
    intros n H.
    induction H.
    simpl.
    repeat constructor.
    simpl.
    rewrite pred_of_addition_ten.
    rewrite x_nine_times.
    apply mulnine_add.
    apply mulnine_mulnine.
    apply IHle.
    apply pow_ten_gt_zero.
Qed.
