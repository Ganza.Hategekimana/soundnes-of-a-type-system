Theorem trans_eq : forall (X:Type) (n m o : X),
    n = m -> m = o -> n = o.
Proof.
    intros X n m o eq1 eq2.
    rewrite -> eq1.
    rewrite -> eq2.
    reflexivity.
Qed.


Example trans_eq_example : forall (a b c : nat),
    a = b ->
    b = c ->
    a = c.
Proof.
    intros a b c eq1 eq2.
    transitivity b.
    apply eq1.
    apply eq2.
Qed.

Example trans_eq_examplep : forall (a b c : nat),
    a = b ->
    b = c ->
    a = c.
Proof.
    intros a b c eq1 eq2.
    transitivity b.
    apply eq1.
    apply eq2.
Qed.

(*injectivité de S*)
Theorem S_injective : forall (n m : nat),
    S n = S m ->
    n = m.
Proof.
    intros n m H1.
    assert (H2: n = pred (S n)). 
    reflexivity.
    rewrite H2.
    rewrite H1.
    simpl.
    reflexivity.
Qed.

Theorem S_injectivep : forall (n m : nat),
    S n = S m ->
    n = m.
Proof.
    intros n m H1.
    injection H1 as Hnm.
    apply Hnm.
Qed.

Theorem discriminate_ex1 : forall (n m : nat),
    false = true ->
    n = m.
Proof.
    intros n m contra.
    discriminate contra.
Qed.


Definition square n := n * n.

Lemma square_mult : forall n m, square (n * m) = square n * square m.
Proof.
    intros n m.
    simpl.
    transitivity b.
    assert (A1 : square n = n * n).
    reflexivity.
    rewrite A1.
    assert (A2 : square m = m * m).
    reflexivity.
    rewrite A2.
Qed.

(* TOY Language*)

(*Definition*)
(*C = constant, P = "+"*)
Inductive tm : Type :=
| C : nat -> tm 
| P : tm -> tm -> tm. 

(*Evaluation*)
Fixpoint evalF (t : tm) : nat :=
    match t with
    | C n => n
    | P t1 t2 => (evalF t1) + (evalF t2)
    end.

Eval compute in evalF (P (C 7) (C 9)).

(*BIG STEP EVALUATION*)
Reserved Notation "t '==>' n" (at level 50, left associativity).

Inductive eval : tm -> nat -> Prop :=
| E_Const : forall n,
        C n ==> n
| E_Plus : forall t1 t2 n1 n2,
        t1 ==> n1 ->
        t2 ==> n2 ->
        P t1 t2 ==> (n1 + n2)

where "t '==>' n" := (eval t n).

(*SMALL STEP EVALUATION*)

Inductive step : tm -> tm -> Prop :=
| ST_PlusConstConst : forall n1 n2,
        P (C n1) (C n2) --> C (n1 + n2)
| ST_Plus1 : forall t1 t1p t2,
        t1 --> t1' ->
        P t1 t2 --> P t1p t2
| ST_Plus2 : forall n1 t2 t2p,
        t2 --> t2p ->
        P (C n1) t2 --> P (C n1) t2p

where " t '-->' " : = (step t tp).


Example test_step_2 :
    P
     (C 0)
     (P
       (C 2)
       (P (C 1) (C 3)))
    -->
    P
     (C 0)
     (P
       (C 2)
       (C 4)).
Proof.
    Admitted.

Example test_step_1 :
    P
     (P (C 1) (C 3))
     (P (C 1) (C 3))
     -->
    P
     (C 4)
     (P (C 2) (C4)).
