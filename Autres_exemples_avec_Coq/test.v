Section Principe_de_la_demonstration_mathematique.
   Variable A B C : Prop.
   Goal (A -> B) /\ (B -> C) -> (A -> C).
   Proof.
      intros H.
      elim H.
      intros H1.
      intros H2.
      intros H3.
      apply H2.
      apply H1.
      exact H3.
   Qed.
End Principe_de_la_demonstration_mathematique.


Section Proprietes_de_l_addition.
   Inductive nat : Type :=
   | zero : nat
   | succ : nat -> nat.
   
   Fixpoint add (x y : nat) : nat :=
   match x with
   | zero => y
   | succ x' => succ (add x' y) 
   end.
   
   Lemma add_succesor (x y : nat) : add x (succ y) = succ (add x y).
   Proof.
      induction x.
      - auto. (*equiv à simpl. reflexivity. *)
      - simpl. rewrite IHx. reflexivity.
   Qed. 
   
   Lemma add_zero (x : nat) : add x zero = x.
   Proof.
      induction x.
      - auto. (*equiv à simpl. reflexivity. *)
      - simpl. rewrite IHx. reflexivity.
   Qed.
   
   Lemma add_comutativite (x y : nat) : add x y = add y x.
   Proof.
      induction x.
      - simpl. rewrite add_zero. reflexivity.
      - simpl. rewrite IHx. rewrite add_succesor. reflexivity.
   Qed.
   
   Lemma add_associativite (x y z : nat) : add (add x y) z = add x (add y z).
   Proof.
      induction x.
      - auto. (*equiv à simpl. reflexivity. *)
      - simpl. rewrite IHx. reflexivity.
   Qed.
   
End Proprietes_de_l_addition.

Section Proprietes_de_la_multiplication.
   Fixpoint mult (x y : nat) : nat :=
   match x with
   | zero => zero
   | succ x' => add y (mult x' y)
   end.
   
   Lemma mult_zero (x : nat) : mult x zero = zero.
   Proof.
      induction x.
      - auto. (*equiv à simpl. reflexivity. *)
      - simpl. rewrite IHx. reflexivity.
   Qed.
   
   Lemma mult_succesor (x y : nat) : mult x (succ y) = add (mult x y) x. 
   Proof.
      induction x.
      - auto. (*equiv à simpl. reflexivity. *)
      - simpl. rewrite add_succesor. rewrite IHx. rewrite add_associativite. reflexivity.
   Qed.
   
   Lemma mult_comutativite (x y : nat) : mult x y = mult y x.
   Proof.
      induction x.
      - simpl. rewrite mult_zero. reflexivity.
      - simpl. rewrite add_comutativite. rewrite mult_succesor. rewrite IHx. reflexivity.
   Qed.
   
   Lemma mult_associativite (x y z : nat) : mult (mult x y) z = mult x (mult y z).
   Proof.
      induction x.
      - auto. (*equiv à simpl. reflexivity. *)
      - simpl. (* ------------- ICI ------------- *)
      
   (*Lemma mult_distributivite (x y z : nat) : mult x (add y z) = add (mult x y) (mult x z).
   Proof.
      induction x.
      - auto. (*equiv à simpl. reflexivity. *)
      - ------------- ICI ------------- *)
   
End Proprietes_de_la_multiplication.