# Proving the Soundness of Type Systems  
project pour le cours d'outils formels avancés  

## Description  
Dans ce projet on s'est interessés à la théoris de types et plus spécifiquement à comment prouver le système de typage d'un langage en utilisant l'assistant de preuve Coq.  

On a commencé par se familiariser avec Coq en essayant d'implémenter différentes preuves et définitions mathématiques (On peut en retrouver quelques unes dans le dossier "Autres\_exemples\_avec\_Coq" )

Ensuite on s'est interressés au langage simply-typed lambda calculus dans un premier temps en utilisant la sémantique opérationelle small-steps (fichier STLC\_smallstep.v) puis en utilisant la sémantique opérationelle big-steps avec la stratégie d'évaluation call-by-value (fichier STLC\_bigstep.v).

## Familiarisation avec l'outil

Bien qu'il ne soit pas aussi populaire que des languages comme python, c++ ou javascript, il existe des sources intéressantes pour connaître Coq et se familiariser avec l'outil.

### Introduction
En terme de documentation, [Software foundation](https://softwarefoundations.cis.upenn.edu/) est une bonne base qui introduit l'outil et spécialise son utilisation pour l'analyse et l'évaluation de languages.

### Documents
Au niveau des documents pdf, nous avons [Coqart](https://www.labri.fr/perso/casteran/CoqArt/) qui est un ouvrage écrit par des passionné de l'outils qui prènnent un approche plus centralisé sur ce qu'est l'outil et ce qu'il peut faire (il est recommander de lire cet ouvrage par morceau). Pour les plus préssés, il y a aussi le document [Coq in a hurry](https://cel.archives-ouvertes.fr/inria-00001173) qui présente les éléments essentiel pour commencer à écrire et faire des démonstrations sur Coq.

### Vidéos
Il existe une série de Vidéos qui s'inspirent de Software foundation et qui se trouve sur [Youtube](https://www.youtube.com/watch?v=BGg-gxhsV4E&list=PLre5AT9JnKShFK9l9HYzkZugkJSsXioFs).

### Jeux
En terme plus ludique, vous pourrez trouver des défis sur Coq dans la plateforme [CodeWars](https://www.codewars.com/). CodeWars est aussi une plateforme qui propose des défis pour plein d'autres langages.


## Résultats et améliorations possibles  
Pour la sémantique big-steps, on a réussi à prouver à la main la préservation et la stabilité du typage dans la substitution, mais par manque de temps on à pas fini la preuve de la progression.  
Pour ce qui est de l'implémentation des preuves dans Coq elles ne sont pas terminée principalement parce qu'on n'as pas réussi à touver queles tactiques nous permetaient d'implémenter certaines étapes des preuves. Mais une fois les bonnes tactiques trouvées pour ces étapes les preuvent devraient être rappidement finies.  
Une ammélioration possible du projet (en dehors du fait de terminer les preuves) serait de modifier l'implémentation du type Value, car pour le moment elle pose quelques problèmes de compatibilité avec les différentes règles implémentées et il faut parfois "tricher" en remplaçant une Value par un Term dans les règles pour que les définitions soient validées.

## Principales références  
- The Software Foundations series of books 
- Cornell's CS4110 course
- Types and Programming Languages  de Benjamin C. Pierce
- Coinductive big-step operational semantics de Xavier Leroy et Hervé Grall
