(* Simply-typed lambda calculus with call-by-value big-steps semantics *)

(* ----------------------------------------------------------------------------------------------------- *)
(* ---------------------------------------- FORMALISATION ---------------------------------------- *)
(* ----------------------------------------------------------------------------------------------------- *)


(* ------------------------------- Syntax ------------------------------- *)

Inductive Ttype : Type :=
| Unit : Ttype
| Function : Ttype -> Ttype -> Ttype.

Definition Var := Prop.

Inductive Term : Type :=
| t : Term
| variable : Var -> Term
| abstraction : Var -> Ttype -> Term -> Term
| application : Term -> Term -> Term.

Inductive Value : Term -> Type :=
| value : Value t
| abs_value : forall (x : Var) (T : Ttype) (t : Term), Value (abstraction x T t). 

Definition Context := Var -> option Ttype.
Definition EmptyContext : Context := fun _ => None.


Lemma eqVariables (x1 x2 : Var) : 
(* equality between two variables *)
   {x1 = x2} + {x1 <> x2}.
Proof.
Admitted.

Definition addContext (G : Context) (x : Var) (T : Ttype) : Context :=
   fun xp => if eqVariables x xp then Some T else G xp.
   
(* ------------------------------- Typing ------------------------------- *)

Inductive Typing : Context -> Term -> Ttype -> Prop :=
| TVar : forall (G : Context) (x : Var) (T : Ttype),
              G x = Some T ->
              Typing G (variable x) T
|TApp : forall (G : Context) (t1 t2 : Term) (T11 T12 : Ttype),
             Typing G t1 (Function T11 T12) -> Typing G t2 T11 ->
             Typing G (application t1 t2) T12
|TAbs : forall (G : Context) (x : Var) (t2 : Term) (T1 T2 : Ttype),
             Typing (addContext G x T1) t2 T2 -> 
             Typing G (abstraction x T1 t2) (Function T1 T2).

(* ----------------------------- Evaluation ----------------------------- *)

Fixpoint substitution (x : Var) (t1 t2 : Term) : Term :=
(* substitution of the variable x by t2 in t1 *)
match t1 with
| t => t
| variable xp => if eqVariables x xp then t2 else t1
| abstraction xp T t3 => abstraction xp T (if eqVariables x xp then t3 else substitution x t3 t2)
| application t3 t4   => application (substitution x t3 t2) (substitution x t4 t2)
end.

Inductive Evaluation : Term -> Term -> Prop :=
| EAbs : forall (x : Var) (T : Ttype) (t : Term),
                 Evaluation (abstraction x T t) (abstraction x T t) 
|EApp : forall (x : Var) (T : Ttype) (t1 t2 tp v vp : Term),
                 Evaluation t1 (abstraction x T tp) -> Evaluation t2 vp
                 -> Evaluation (substitution x tp vp) v ->
                 Evaluation (application t1 t2) v.

(* ----------------------------- Divergence ----------------------------- *)

CoInductive Divergence : Term -> Prop :=
| DAppL : forall (t1 t2 : Term),
                 Divergence t1 -> Divergence (application t1 t2)
| DAppR : forall (t1 t2 v : Term),
                 Evaluation t1 v -> Divergence t2 -> 
                 Divergence (application t1 t2)
| DAppAbs : forall (x : Var) (t1 t2 tp v : Term) (T : Ttype),
                  Evaluation t1 (abstraction x T tp) -> Evaluation t2 v 
                  -> Divergence (substitution x tp v) ->
                  Divergence (application t1 t2).

(* ----------------------------------------------------------------------------------------------------- *)
(* --------------------------------------- TYPE SOUNDNESS -------------------------------------- *)
(* ----------------------------------------------------------------------------------------------------- *)


Lemma typing_and_substitution (G : Context) (x : Var) (t1 t2 : Term) ( T T1 : Ttype) :
 Typing G (variable x) T1 /\ Typing G t1 T /\ Typing G t2 T1 -> Typing G (substitution x t1 t2) T.

Proof.
   intros.
   elim H.
   intros H1 Hp.
   elim Hp.
   intros H2 H3.
   induction t1.
   
   (* case where t1 is a term *)
    - simpl. 
      exact H2.
      
    (* case where t1 is a variable *)
    - remember (eqVariables v x) as e.
      induction e.
      simpl.
      (* ... *)
      (* Find wich tactics allows to break if...then...else in cases
      then simply developp first case x=v , second case not(x=v) *)
      
   (* case where t1 is an abstraction *)
    - simpl.
      remember (eqVariables v x) as e.
      induction e.
      (* ... *)
      (* Find wich tactics allows to break if...then...else in cases
      then simply developp first case x=v , second case not(x=v) *)

    (* case where t1 is an application *)
    - simpl.
      (* ... *)
      (* Find wich tactics allows to rewrite the substitution terms inside 
      the application *)
      
Admitted.

Lemma preservation (G : Context) (x : Var) (t v : Term) (T : Ttype) :
Typing G t T /\ Evaluation t v -> Typing G v T.
Proof.
   intros.
   elim H.
   intros H1 H2.
   induction t. 
   (* case where t is a term *)
    - 
    
   (* case where t1 is a variable *)
    - 
      (* ... *)
      (* Show the link between  Evaluation x v and substitution x t v
      then : Typing G (variable x) T -> Typing G (substitution x (variable x) v) T
      -> Typing G v T *)

   (* case where t1 is an abstraction *)
    - replace v with (abstraction v0 t0 t1).
      exact H1. 
      (* ... *)
      (* Show the link between  Evaluation x v and substitution x t v*) 
   
   (* case where t1 is an application *)
    - 
Admitted.

Lemma progress (G : Context) (t : Term) (T : Ttype) :
Typing G t T -> Divergence t \/ (exists v, Evaluation t v).
Proof.
Admitted.

