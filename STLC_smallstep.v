(* Simply-typed lambda calculus with small-steps semantics *)

(* ----------------------------------------------------------------------------------------------------- *)
(* ---------------------------------------- FORMALISATION ---------------------------------------- *)
(* ----------------------------------------------------------------------------------------------------- *)


(* ------------------------------- Syntax ------------------------------- *)

Inductive Ttype : Type :=
| Unit : Ttype
| Function : Ttype -> Ttype -> Ttype.

Definition Var := Prop.

Inductive Term : Type :=
| t : Term
| variable : Var -> Term
| abstraction : Var -> Ttype -> Term -> Term
| application : Term -> Term -> Term.

Inductive Value : Term -> Prop :=
| value : Value t
| abs_value : forall (x : Var) (T : Ttype) (t : Term), Value (abstraction x T t). 

Definition Context := Var -> option Ttype.
Definition EmptyContext : Context := fun _ => None.


Lemma eqVariables (x1 x2 : Var) : 
(* equality between two variables *)
   {x1 = x2} + {x1 <> x2}.
Proof.
Admitted.

Definition addContext (G : Context) (x : Var) (T : Ttype) : Context :=
   fun xp => if eqVariables x xp then Some T else G xp.

(* ------------------------------- Typing ------------------------------- *)

Inductive Typing : Context -> Term -> Ttype -> Prop :=
| TVar : forall (G : Context) (x : Var) (T : Ttype),
              G x = Some T ->
              Typing G (variable x) T
|TApp : forall (G : Context) (t1 t2 : Term) (T11 T12 : Ttype),
             Typing G t1 (Function T11 T12) -> Typing G t2 T11 ->
             Typing G (application t1 t2) T12
|TAbs : forall (G : Context) (x : Var) (t2 : Term) (T1 T2 : Ttype),
   Typing (addContext G x T1) t2 T2 -> 
   Typing G (abstraction x T1 t2) (Function T1 T2).

(* ----------------------------- Evaluation ----------------------------- *)

Fixpoint substitution (x : Var) (t1 t2 : Term) : Term :=
(* substitution of the variable x by t2 in t1 *)
match t1 with
| t => t
| variable xp => if eqVariables x xp then t2 else t1
| abstraction xp T t3 => abstraction xp T (if eqVariables x xp then t3 else substitution x t3 t2)
| application t3 t4   => application (substitution x t3 t2) (substitution x t4 t2)
end.

Inductive Evaluation : Term -> Term -> Prop :=
| EApp1 : forall (t1 t1p t2 : Term),
                 Evaluation t1 t1p ->
                 Evaluation (application t1 t2) (application t1p t2)
| EApp2 : forall (t1 t2 t2p : Term),
                 Evaluation t2 t2p ->
                 Evaluation (application t1 t2) (application t1 t2p)
|EAppAbs : forall (x : Var) (T : Ttype) (t1 t2 : Term),
                  Evaluation (application (abstraction x T t1) t2) (substitution x t1 t2).

(* ----------------------------------------------------------------------------------------------------- *)
(* --------------------------------------- TYPE SOUNDNESS -------------------------------------- *)
(* ----------------------------------------------------------------------------------------------------- *)

Lemma preservation (G : Context) (x : Var) (t1 t2 : Term) (T : Ttype) :
Evaluation t1 t2 /\ Typing G t1 T -> Typing G t2 T.
Proof.
Admitted.

Lemma progress (G : Context) (x : Var) (t1 : Term) (T : Ttype) :
Typing G t1 T -> Value t1 \/ (exists t2, Evaluation t1 t2).
Proof.
Admitted.